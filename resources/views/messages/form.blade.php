<div class="form">
    {{ Form::open(['route' => 'messages.store'])}}
        <div class="form-group required">
            {!! Form::label('name', 'Name', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-12">
                {!! Form::text('name', $value = null, ['class' => $errors->has('name') ? 'form-control is-invalid' : 'form-control', 'placeholder' => 'name']) !!}
            </div>
            @if ($errors->has('name')) <p class="help-block col-lg-12 text-danger">{{ $errors->first('name') }}</p> @endif
        </div>
        <div class="form-group required">
            {!! Form::label('email', 'Email', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-12">
                {!! Form::email('email', $value = null, ['class' => $errors->has('email') ? 'form-control is-invalid' : 'form-control', 'placeholder' => 'email']) !!}
            </div>
            @if ($errors->has('email')) <p class="help-block col-lg-12 text-danger">{{ $errors->first('email') }}</p> @endif
        </div>
        <div class="form-group required">
            {!! Form::label('textarea', 'Message', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-12">
                {!! Form::textarea('message', $value = null, ['class' => $errors->has('message') ? 'form-control is-invalid' : 'form-control', 'rows' => 3, 'placeholder' => 'message']) !!}
            </div>
            @if ($errors->has('message')) <p class="help-block col-lg-12 text-danger">{{ $errors->first('message') }}</p> @endif
        </div>
        <div class="form-group">
            <div class="col-lg-12 col-lg-offset-2">
                {!! Form::submit('Submit', ['class' => 'btn btn-lg btn-info pull-right'] ) !!}
            </div>
        </div>
    {{ Form::close() }}
</div>
