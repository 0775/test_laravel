<div class="messages col-lg-12">
    @foreach ($aMessages as $message)
        <div class="item">
            <div class="info">
                <span class="user">{{ $message->name }}</span>, {{ $message->created_at->format('d.m.Y H:i:s') }} {{ $message->email }}
            </div>
            <div class="full">
                {{ $message->message }}
            </div>
        </div>
    @endforeach
</div>
