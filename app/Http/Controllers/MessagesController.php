<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Message;

class MessagesController extends Controller
{
    public function index()
    {
        $view['aMessages'] = Message::orderBy('created_at', 'DESC')->get();
        return view('messages.index',$view);
    }

    public function store(Request $request)
    {
        $message = new Message;
        $request->validate($message->rules);
        $message->create($request->all());
        return redirect()->route('messages.index');
    }
}
