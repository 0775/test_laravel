<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    public $rules = [
        'name' => 'required',
        'email' => 'required|email',
        'message' => 'required',
    ];
    protected $fillable = ['name','email','message'];
}
